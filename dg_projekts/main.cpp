#define GLEW_STATIC
#include <GL/glew.h>
#define GL_SILENCE_DEPRECATION 1
#include <GLFW/glfw3.h>
#include <math.h>
#include <iostream>

using namespace std;

const GLfloat WIDTH = 800, HEIGHT = 600;

void keyCallback(GLFWwindow *window, int keyCode, int scancode, int action, int mods);
void characterCallback(GLFWwindow *window, unsigned int keyCode);
void characterModCallback(GLFWwindow *window, unsigned int keyCode, int modifierKey);
void cursorPositionCallback(GLFWwindow *window, double xPos, double yPos);
void cursorEnterCallback(GLFWwindow *window, int entered);
void mouseButtonCallback(GLFWwindow *window, int button, int action, int mods);
void windowCloseCallback(GLFWwindow *window);

int main(void)
{
    // Initialize the library
    if ( !glfwInit() )
    {
        return -1;
    }
    
    GLFWwindow *window = glfwCreateWindow(WIDTH, HEIGHT, "Lauris Stekels DG projekts", nullptr, nullptr);
    // Cursor type and look
//    GLFWcursor *cursor = glfwCreateStandardCursor(GLFW_HAND_CURSOR);
    // attach the cursor to created window
//    glfwSetCursor(window, cursor);
        
    // Mouse cursor position callback for the whole screen
    glfwSetCursorPosCallback(window, cursorPositionCallback);
    // Cursor mode, default is normal
//    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    
    // cursor enter callback for when the cursor enters created window
    glfwSetCursorEnterCallback(window, cursorEnterCallback);
    glfwSetMouseButtonCallback(window, mouseButtonCallback);
    
    
    // Key callback
    glfwSetKeyCallback(window, keyCallback);
    // Proper polling and key press handling
    glfwSetInputMode(window, GLFW_STICKY_KEYS, GLFW_TRUE);

    // Character callback
    //glfwSetCharCallback(window, characterCallback);

    // Character callback with mods
    //glfwSetCharModsCallback(window, characterModCallback);
    
    // Fix for retina displays
    int screenWidth, screenHeight;
    // get the actual width and height in pixels
    glfwGetFramebufferSize(window, &screenWidth, &screenHeight);
    
    if (!window)
    {
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glewExperimental = GL_TRUE;

    // This part converts from normalized -1 to 1 coordinates to pixel coordinates that are made according to window size
    glViewport(0.0f, 0.0f, screenWidth, screenHeight);
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity(); // Put back at (0,0,0) because of the matrix transform cumulation
    glOrtho(0, WIDTH, 0, HEIGHT, 0, 1); // This sets my coordination system
    glMatrixMode(GL_MODELVIEW); // Default matrix mode defines how my objects are transformed in this world
    glLoadIdentity(); // Does the same thing as previously

    // Main loop until the window is closed
    while ( !glfwWindowShouldClose( window ) )
    {
        glClear( GL_COLOR_BUFFER_BIT );
        // RENDER HERE
        
        // Swap front and back buffers
        glfwSwapBuffers( window );
        // Poll for and process events
        glfwPollEvents();
    }
    
    glfwTerminate();
    
    return 0;
}

void keyCallback( GLFWwindow *window, int keyCode, int scancode, int action, int mods )
{
    if (keyCode == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    }
    if (keyCode == GLFW_KEY_ENTER && keyCode == GLFW_KEY_LEFT_ALT)
    {
        
    }
}

void characterCallback(GLFWwindow *window, unsigned int keyCode)
{
    // TODO
}

void characterModCallback(GLFWwindow *window, unsigned int keyCode, int modifierKey)
{
    // TODO
}

void cursorPositionCallback(GLFWwindow *window, double xPos, double yPos)
{
    // TODO
}

void cursorEnterCallback(GLFWwindow *window, int entered)
{
    if ( entered )
    {
        cout << "Entered window" << endl;
    }
        else
    {
        cout << "Exits window" << endl;
    }
}

void mouseButtonCallback(GLFWwindow *window, int button, int action, int mods)
{
    switch ( button )
    {
        case GLFW_MOUSE_BUTTON_RIGHT:
            cout << "Right button pressed: " << action << endl;
            break;
        case GLFW_MOUSE_BUTTON_LEFT:
            cout << "Left button pressed: " << action << endl;
            break;
        default:
            break;
    }
    
}

void windowCloseCallback(GLFWwindow *window)
{
    
}
